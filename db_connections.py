import allure
import mysql.connector


class DbConnectionHelper:

    def __init__(self):
        self.host = '127.0.0.1'
        self.user = 'root'
        self.database = 'litecart'
        self.password = ''
        self.conn = None
        self.cur = None

    def create_connection(self):
        with allure.step('Устанавливаем соединение с БД'):
            db = mysql.connector.connect(
                host=self.host,
                database=self.database,
                user=self.user,
                password=self.password)

            self.conn = db
            self.cur = self.conn.cursor()

    def check_username_changed(self):
        with allure.step('Проверянм, что имя изменено в бд'):
            self.cur.execute('select username from lc_users')
            username = self.cur.fetchall()
            new_username = 'admin'
            for i in username[0]:
                if i == new_username:
                    assert True
                else:
                    assert False
