import time

import allure

from locators.cart_page_locator import CartPageLocators
from pages.BasePage import BasePage


class CartPageHelper(BasePage):
    def edit_quantity_ducks(self, num_ducks: int):
        with allure.step(f'Ставим количество уток "{num_ducks}"'):
            summary = self.find_element(
                CartPageLocators.SUM_TEXT)
            sum_for_one_duck = summary.text[1::]
            self.driver.execute_script(
                f'document.getElementsByName('
                f'"quantity")[0].setAttribute("value" , "{num_ducks}")')
            update_button = self.find_element(
                CartPageLocators.UPDATE_BUTTON)
            update_button.click()
            time.sleep(3)
        with allure.step('Проверяем, что уточки добавились и '
                         f'цена верная для {num_ducks} уточек'):
            ducks_quantity = self.find_element(
                CartPageLocators.QUANTITY_NUMBER_TEXT)
            total_sum = self.find_element(
                CartPageLocators.TOTAL_SUM_TEXT)
            total = num_ducks * int(sum_for_one_duck)
            assert ducks_quantity.text == f'{num_ducks}'
            assert total_sum.text == f'${total}.00'

    def remove_duck_from_bucket(self):
        with allure.step('Удаляем уточек'):
            remove_button = self.find_element(
                CartPageLocators.REMOVE_BUTTON)
            remove_button.click()
        with allure.step('Проверяем что корзина пуста'):
            no_item = self.find_element(
                CartPageLocators.EMPTY_CART_TEXT)
            assert no_item.text == 'There are no items in your cart.'
