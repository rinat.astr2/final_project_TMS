from selenium import webdriver
import pytest

from db_connections import DbConnectionHelper


@pytest.fixture
def browser():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    driver = webdriver.Chrome(chrome_options=chrome_options)
    driver.implicitly_wait(60)
    yield driver
    driver.quit()


@pytest.fixture
def db_client():
    db = DbConnectionHelper()
    db.create_connection()
    yield db
    db.conn.close()
